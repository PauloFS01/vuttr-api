var mongoose = require('mongoose');

module.exports = function (app) {
    var tools = {};
    var model = mongoose.model('Tools');

    tools.save = function (req, res) {
        let newTool = req.body;
        model.create(newTool)
        .then(function (tool) {
            res.status(200).json(tool);
        }, function (err) {
            res.status(500).json(err);
        })        
    };

    tools.getAll = async function (req, res) {
        if ( req.query.tags_like ) {
            let target = new RegExp(req.query.tags_like);          
            let result = await model.find({ 'tags': { $regex:  target, $options: 'i' }});
            res.status(200).json(result);
        } else if ( req.query.q ) {
            let target = new RegExp (req.query.q);
            let result = await model.find({ $or: [
                {'title': { $regex:  target, $options: 'i' }}, 
                {'tags': { $regex:  target, $options: 'i' }}, 
                {'description': { $regex:  target, $options: 'i' }}
            ]});            
                 
            res.status(200).json(result);
        } else {
            model.find()
            .then(function (tools) {
               res.status(200).json(tools);
            }, function (err) {
                res.status(500).json(err);
            })
        }
    };

    tools.delete = async function (req, res) { 
        try {
            let result = await model.deleteOne({'_id': req.params.id});
            res.sendStatus(200);
        } catch (error) {
            res.sendStatus(500);            
        }
    };
    return tools;
}
