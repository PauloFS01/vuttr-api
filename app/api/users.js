var mongoose = require('mongoose');

module.exports = function (app) {
    var users = {};
    var model = mongoose.model('Users');
    var defaultUser =  {}

    users.save = async function (req, res) {
        defaultUser.user = 'admin';
        defaultUser.password = '123456'
        try {
            let name = await nameVerify (defaultUser.user);
            if (!name.length) {
                let result =  await model.create(defaultUser);
                res.status(200).json('User admin registred');
            } else {
               res.status(401).json('Username not available');
            }
        } catch (error) {
            res.status(500).json(error);
        }
    };

    async function nameVerify (name) {
        let result = await model.find({'user': name});        
        return result;
    };

    return users;
}