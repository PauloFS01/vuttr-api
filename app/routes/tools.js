module.exports = function (app) {

    var {tools, auth } = app.api;

    app.route('/auth')
    .post(auth.authenticate);

    app.route('/tools')
    .get(auth.verify, tools.getAll)
    .post(auth.verify, tools.save);

    app.route('/tools/:id')
    .delete(auth.verify, tools.delete);
}