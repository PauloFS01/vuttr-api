var mongoose = require('mongoose');

var schema = mongoose.Schema({
    
    title: { type: String },
    link: { type: String },
    description: { type: String },
    tags: { type: Array }
    
}, {
    versionKey: false 
});

mongoose.model('Tools', schema);