module.exports = function () {
    var expired = {}

    expired.token = function (seconds) {
        const expiredDate = new Date();
        expiredDate.setSeconds(expiredDate.getSeconds() + seconds);
        return expiredDate;        
    }

    return expired;
};